| Home | Debian Install | Windows notes |


This page is divided into 5 categories:
1#    Debian GNU/Linux 8 / Jessie installation/configuration guide - MATE Desktop  - with X11.
2#    Debian GNU/Linux 8 / Jessie - Server configuration - headless, no X11.
3#    RaspberryPi configuration - headless, no X11.
4#    Android links / notes. 
5#    Other Linux/Unix devices configurations.

Page file size is: 1 043 036 bytes / 1018.6 kB and was last updated on: Wednesday, 05 October 2016
Press Ctrl + F5 / Shift + Ctrl + F5 in your web browser to force cache purge and get up to date version.


What's new:
- added SCP, SSHFS, SSHFS on Windows - Friday, 23 September 2016
- added  nginx.org GPG key expiration & update, Android links updated Wire, Telegram, Three inTouch, FreeVoipDeal - Tuesday, 20 September 2016
- updated mkvmerge notes split/merge mkv, minor amendments - Tuesday, 02 August 2016
- added  NVIDIA Proprietary Driver Installation, Change Resolution = dpi - Saturday, 09 July 2016
- added/updated Firefox ESR, HandBrake instalation / deadbeef music player, Youtube-dl, Telegram IM - Saturday, 02 July 2016
- added System Printing / Text-mode WWW Browsers  - Thursday, 23 June 2016
- updated Youtube-dl - Wednesday, 22 June 2016
- updated Viber installation - Monday, 20 June 2016
- updated/added Youtube-dl, MakeMKV key updated / Upgrading OpenWRT, ClamAV - AntiVirus, Tor-Browser - Sunday, 19 June 2016 
- updated deadbeef music player,  Youtube-dl - Wednesday, 15 June 2016
- added/updated  X Server - X11 - X, Upgrade BIOS on Dell / Youtube-dl, MakeMKV, Install Flash support - Friday, 10 June 2016
- added Upgrade Pepper Flash Player, Multiarch support, ClamAV - Sunday, 29 May 2016
- updated Telegram.Deskop shortcut - Sunday, 29 May 2016
- updated Telegram installation - Wednesday, 25 May 2016
- updated Basic Commands - Sunday, 22 May 2016
- added Youtube-dl - Sunday, 22 May 2016
- added SHFS_Windows 8 / 7 - Friday, 20 May 2016
- updated Basic Commands - Wednesday, 18 May 2016
- updated Essential software first - Saturday, 14 May 2016
- added Shell and Basic Commands - Friday, 13 May 2016
- added Skype installation - Tuesday, 10 May 2016
- added Debian Long Term Support - LTS info - Tuesday, 26 April 2016
- added Openarena fast-paced 3D first-person shooter installation - Saturday, 09 April 2016
- added installation of cdrtools 3.02~a06-1, cdrdao, CDemu, MakeMKV, MKVToolNix - Friday, 08 April 2016
- added LightDM - display users on login - Tuesday, 29 March 2016
- added removing CA certificate / Jigdo - Saturday, 19 March 2016
- added TeamViewer QuickSupport / WINE installation - Tuesday, 15 March 2016
- added instalation of Flash player / Chromium browser - Monday, 14 March 2016
- added installation of MiniDLNA, MPD (Music player daemon) / MPC - Thursday, 10 March 2016        
- this page was created - Wednesday, 09 March 2016


------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
1#    Debian GNU/Linux 8 / Jessie Installation Guide - MATE Desktop - Index
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
#    Alternative to Windows = Linux - Debian Free OS on your PC
#    What will you need - Itinerary
#    Installation process
#    SSH
#    Root login
#    Nano Editor tutorial
#    Configure sources.list
#    Add Multiarch support
#    Debian Long Term Support - LTS
#    Unattended Upgrades
#    Essential software first
#    Basic commands - B.1. Shell and Basic Commands
#    The Linux Documentation Project (LDP)  Books about Linux - linuxbook -booklinux
#    SUDOers file - visudo
#    Color Bash Prompt - root
#    Folders for downloaded Installation files *.deb / *.tar.* / and from GitHub
#    Color Bash Prompt - user
#    Customize nano text editor to display line numbers
#    Mosh - (mobile shell)
#    Shellinabox (web ssh)
#    Install the missing firmware for the Wi-Fi card BCM43xx
#    Install the missing firmware for the Wi-Fi card Intel 6250
#    Firmware for Intel Wireless cards supported by the iwlegacy/iwl3945 and iwlwifi/iwlagn drivers.
#    Binary firmware for various drivers in the Linux kernel / Radeon error
#    Broadcom bcma0:0 failed
#    Binary firmware for Atheros wireless cards
#    Binary firmware for Realtek wired/wifi/BT adapters
#    Processor microcode firmware for Intel CPUs
#    NVIDIA Proprietary Driver - NvidiaGraphicsDrivers
#    Change Resolution - dpi (dots per inch)
#    NetworkManager
#    Bluetooth - blueman (Gtk2)
#    System Printing
#    Update USBID / PCIID
#    How to find version of your OS
#    Other usefull info commands
#    Add New User
#    LightDM - display users on login
#    Securing SSHD
#    SSH config stanza 
#    Reverse SSH Tunnel
#    SSH WEB Tunnel Manager - sshwebtunnel - sshhttptunnel - sshhttp
#    FTP - File Transfer Protocol
#    FTPS - FTP over SSL also known as FTPES, FTP-SSL, S-FTP
#    SFTP - Simple File Transfer Protocol
#    SFTP - SSH File Transfer Protocol
#    Secure copy or SCP
#    SSHFS (SSH Filesystem)
#    SSHFS on Windows 8 / 7
#    ClamAV - AntiVirus
#    Install Text-mode WWW Browsers
#    Install Tor-Browser
#    Install Chromium Web browser
#    Install SeaMonkey all-in-one internet application suite.
#    Iceweasel -> Firefox ESR
#    Install Flash support - PepperFlashPlayer
#    Upgrade Flash player - UpgradePepperFlashPlayer
#    Immediate assistance: TeamViewer QuickSupport
#    TeamViewer_FULL
#    VideoLAN - VLC - multimedia player and streamer
#    libdvdcss / libdvdcss2 / libdvd-pkg - Protected DVD Playback
#    libbluray/libaacs - Protected Blu-Ray Playback
#    How to find if DVD/BD writer supports DVD+R and/or DVD-R / BD .. 
#    Nero Linux 4 Update
#    DVD-Video / DVD-Audio to ISO - dvd2iso
#    dvdbackup /  vobcopy in 3 steps
#    Joerg Schilling cdrtools 3.02~a06-1 (cdrecord, cdda2wav, mkisofs) / cdrecord-ProDVD
#    cdrdao - almost exact copy of the audio CD.
#    CDemu is a software suite designed to emulate an optical drive and disc (including CD-ROMs and DVD-ROMs)
#    Asunder - GUI_mp3 RIPper - riping to wav/mp3/flac/ogg/ - GUI
#    EasyTAG - Mp3tag alternative
#    deadbeef music player - GUI
#    Wine Is Not an Emulator
#    Installing PlayOnLinux - front-end for Wine
#    Install / Configure MiniDLNA
#    MPD (Music player daemon)
#    Clients for MPD (Music player daemon)
#    Speedtest from CLI @speedtest.net
#    MakeMKV one-click solution to convert video that you own into free and patents-unencumbered format that can be played everywhere.
#    MKVToolNix – Matroska tools for Linux/Unix and Windows
#    Install get_iPlayer on Debian Jessie to download BBC iPlayer Video/Audio
#    HandBrake - A tool to convert video from nearly any format to modern codecs
#    Youtube-dl
#    Livestreamer
#    MediaInfo installation/usage
#    Torrent clinet - transmission
#    Firewall - Shorewall - Debian Jessie IPv4
#    Firewall - Shorewall - Debian Jessie IPv6
#    Hard Disk Sentinel 32-bit
#    Hard Disk Sentinel 64-bit
#    Remove a "CA" certificate authority's certificate from a system
#    Add CACert.org  to your system
#    Jigdo download
#    Telegram – a new era of messaging
#    Viber - 64-bit ONLY  - NOT WORKING!
#    Pidgin - free chat client / GTalk - prosody - XMPP
#    Skype - IM
#    Openarena fast-paced 3D first-person shooter
#    Kids_friendly_Games
#    systemd / sysvinit
#    X Server - X11 - X
#    Upgrade BIOS on Dell PC/Laptop from shell/ssh


------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
2#    Debian GNU/Linux 8 / Jessie Server - Index
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
#    BitTorrent CLI clinet - ctorrent
#    BitTorrent CLI clinet - rtorrent
#    BitTorrent CLI clinet - Transmission-cli
#    nginx.org_GPG key expiration & update
#    ProsodyIM - XMPP Server soon


------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
3#    RaspberryPi - Index
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
#    Installing Raspbian OS onto the SD/MicroSD card
#    TVHeadEnd soon
#    Kodi.tv


------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
4#    Android - Index
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
#    Firefox.for.Android
#    NoScript Mobile - Multiprocessing / Android Porting Project for Firefox
#    JuiceSSH - SSH Client
#    BigSMS (Send Long SMS)
#    Telegram.for.Android
#    Wire - Private Messenger
#    Three inTouch
#    FreeVoipDeal Cheap Voip Calls
#    Tethering - settings put global tether_dun_required 0



------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
5#    Other Linux/Unix devices - Index
------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------
#    Upgrading OpenWRT CHAOS CALMER 15.05, r46767 to 15.05.1, r48532 on Linksys E3000
#    Upgrading OpenWRT BARRIER BREAKER (14.07, r42625) to CHAOS CALMER (15.05, r46767) on Linksys E3000
#    Buffalo LinkStation

